#!/usr/bin/env bash

# ** This script will create a reverse proxy in NGINX for the subdomain specified, to point to the specified IP & port
# ** Example Usage  "./nginx.sh ftp 192.168.1.1:8090"   which will create a FTP.base_domain -> 192.168.1.1:8090

############################################################
# Version 1.0 - Initial
# Version 1.1 - Prefix with "http" protocol for IP if needed
# Version 1.2 - Suffix with Base_Domain if needed
############################################################

location=/etc/nginx/sites-available/
template=template.conf
Base_Domain=".proxy.DigitalFox.ca"

alert1=`tput setab 7``tput setaf 1``tput smso`
alert2=`tput setab 2``tput setaf 4``tput smso`
alert3=`tput setab 3``tput setaf 0`
reset=`tput sgr 0`

# Check if arguments passed in. --> "$#" checks the # of arguments.
# Source: http://stackoverflow.com/a/6482403

if [ $# -eq 0 ]
  then
    echo "No arguments supplied"
    exit
fi



function create_reverse_proxy {

    # Include a "http" prefix if missing
    if  [[ ${ip} != http* ]];
    then
        ip="http://${ip}"
    fi
    
    # Appending Base Domain if not present
    if  [[ ${domain} != *${Base_Domain} ]];
    then
        domain="${domain}${Base_Domain}"
    fi
    

    cd ${location}
    
    echo "${alert2} Duplicate from Template Configuration File  ${reset}"
    sudo cp ${location}/${template} ${location}/${domain}.conf
    
    echo "${alert2} Replacing Reverse Proxy URL  ${reset}"
    sudo sed -i "s@foobar.com@${domain}@g" ${location}/${domain}.conf
    
    echo "${alert2} Replacing Server IP behind proxy  ${reset}"
    sudo sed -i "s@http://192.168.0.0@${ip}@g" ${location}/${domain}.conf
    
    echo "${alert2} Creating SymLink to Sites-Enabled Folder  ${reset}"
    sudo ln -s ${location}/${domain}.conf ${location}/../sites-enabled/${domain}.conf

    echo "${alert1} Reloading NGINX service to activate Reverse Proxy Site  ${reset}"
    sudo service nginx reload
    
} # => create_reverse_proxy()


domain=$1
ip=$2
create_reverse_proxy

